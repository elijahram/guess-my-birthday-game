from random import randint

name = input("Hi! What is your name?: ")

for i in range(5):
    month_number = number_between_1_and_12 = randint(1, 12)
    year_number = number_between_1_and_100 = randint(1924, 2004)

    print("Guess " + str(i + 1) +  " : " + name + " were you born on " + str(month_number) + " / " + str(year_number) + " ?")
    response = input("yes or no?: ")
    if i == 5 and response == "no":
        print("I have other things to do. Good bye.")
    elif response == "yes":
        print("I knew it!")
        break
    elif response == "no":
        print("Drat! Lemme try again!")